# gjs-eslintrc

This is a mirror of [GNOME Shell](https://gitlab.gnome.org/GNOME/gnome-shell)'s [ESLint](https://eslint.org/) rules, adapted for GJS development & merged into a single file.
